package ru.tinkoff.ru.seminar.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.model.Weather;

public interface WeatherApi {

    String BASE_URL = "http://api.openweathermap.org";

    @GET("/data/2.5/weather")
    Single<Weather> getWeatherByCity(@Query("q") final String city,
                                     @Query("APPID") final String appId,
                                     @Query("units") final String units,
                                     @Query("lang") final String language);

    @GET("/data/2.5/forecast")
    Single<List<Weather>> getForecastByCity(@Query("q") final String city,
                                            @Query("APPID") final String appId,
                                            @Query("units") final String units,
                                            @Query("lang") final String language);

}
