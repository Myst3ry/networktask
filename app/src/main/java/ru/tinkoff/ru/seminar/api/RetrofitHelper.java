package ru.tinkoff.ru.seminar.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.tinkoff.ru.seminar.BuildConfig;
import ru.tinkoff.ru.seminar.api.deserializer.ForecastJsonDeserializer;
import ru.tinkoff.ru.seminar.api.deserializer.WeatherJsonDeserializer;
import ru.tinkoff.ru.seminar.model.Weather;

import static ru.tinkoff.ru.seminar.api.WeatherApi.BASE_URL;

public final class RetrofitHelper {

    public WeatherApi getApi() {

        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(Weather.class, new WeatherJsonDeserializer())
                .registerTypeAdapter(new TypeToken<List<Weather>>(){}.getType(), new ForecastJsonDeserializer())
                .setLenient()
                .create();

        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG
                        ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE))
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(WeatherApi.class);
    }
}