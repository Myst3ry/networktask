package ru.tinkoff.ru.seminar.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateUtils {

    private static final long UNIX_TIME_MULTIPLIER = 1000L;
    private static final String OUTPUT_DATE_FORMAT = "dd MMMM yyyy";

    public static String convertDate(final long epochDate) {
        final SimpleDateFormat outputFormat = new SimpleDateFormat(OUTPUT_DATE_FORMAT, Locale.getDefault());
        return outputFormat.format(new Date(epochDate * UNIX_TIME_MULTIPLIER));
    }

    private DateUtils() { }
}
