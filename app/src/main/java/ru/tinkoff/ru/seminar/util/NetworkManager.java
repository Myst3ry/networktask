package ru.tinkoff.ru.seminar.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public final class NetworkManager {

    private static BehaviorSubject<Boolean> connectionResult = BehaviorSubject.create();

    public static Observable<Boolean> checkConnection(final Context context) {

        final ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = connectivityManager != null
                ? connectivityManager.getActiveNetworkInfo()
                : null;
        connectionResult.onNext(networkInfo != null && networkInfo.isConnectedOrConnecting());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {

                @Override
                public void onAvailable(Network network) {
                    connectionResult.onNext(true);
                }

                @Override
                public void onLost(Network network) {
                    connectionResult.onNext(false);
                }
            };

            final NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .build();

            if (connectivityManager != null) {
                connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
            }
        } else {
            context.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final boolean isConnectionLost = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,
                            false);
                    connectionResult.onNext(!isConnectionLost);
                }
            }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        return connectionResult;
    }

    private NetworkManager() {

    }
}
