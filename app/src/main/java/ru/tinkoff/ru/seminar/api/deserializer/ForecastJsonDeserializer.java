package ru.tinkoff.ru.seminar.api.deserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.tinkoff.ru.seminar.model.Weather;

public final class ForecastJsonDeserializer implements JsonDeserializer<List<Weather>> {

    @Override
    public List<Weather> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        final List<Weather> weatherList = new ArrayList<>();
        final JsonArray array = json.getAsJsonObject().getAsJsonArray("list");

        for (JsonElement element : array) {
            final JsonObject obj = element.getAsJsonObject();

            final long time = obj.get("dt").getAsLong();
            final float temp = obj.get("main").getAsJsonObject().get("temp").getAsFloat();
            final float speedWind = obj.get("wind").getAsJsonObject().get("speed").getAsFloat();
            final String description = obj.get("weather").getAsJsonArray().get(0).getAsJsonObject()
                    .get("description").getAsString();

            weatherList.add(new Weather(description, time, temp, speedWind));
        }

        return weatherList;
    }
}
