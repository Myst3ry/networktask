package ru.tinkoff.ru.seminar.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.tinkoff.ru.seminar.model.Weather;

public final class WeatherJsonDeserializer implements JsonDeserializer<Weather> {

    @Override
    public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        final JsonObject obj = json.getAsJsonObject();

        final long time = obj.get("dt").getAsLong();
        final float temp = obj.get("main").getAsJsonObject().get("temp").getAsFloat();
        final float speedWind = obj.get("wind").getAsJsonObject().get("speed").getAsFloat();
        final String description = obj.get("weather").getAsJsonArray().get(0).getAsJsonObject()
                .get("description").getAsString();

        return new Weather(description, time, temp, speedWind);
    }
}
